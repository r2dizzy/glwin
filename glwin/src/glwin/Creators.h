/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#pragma once

// THIS FILE ALSO HAS DESTORY FUNCS ASWELL

#include "Window.h"

#include "Library.h"

#include "Threading/Thread.h"

#include <string>
#include <vector>

#if defined( _WIN32 )
#include <shellapi.h>
#endif

#if defined ( __linux__ )
#include <X11/Xlib.h>
#include <X11/Xutil.h>
// I wanted to not include any OpenGL header but it looks we might have to.
#include <GL/glx.h>
#include <GL/gl.h>
#endif

namespace glwinc {

	// Yes, Linux and xlib do not work together when we have a class called window.
	using namespace glwin;

	struct WindowCreateInfo
	{
		std::string Title;
		int Width, Height;
		float X, Y;
		WindowStyle Style = glwin::WindowStyle::NORMAL;
		WindowGraphicsAPI GraphicsAPI = WindowGraphicsAPI::OPENGL;
	};

	//////////////////////////////////////////////////////////////////////////
	// WIN32 WINDOW RESISTER
	//////////////////////////////////////////////////////////////////////////
#if defined( _WIN32 )

	extern void RegisterWindowClass();
	extern void UnregisterWindowClass();

	static Thread s_KeyThread;

	extern LRESULT CALLBACK WindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

#elif defined( __linux__ )

	extern void glwinLinuxInitGLXContext( glwin::Window* wind );

#endif

	/*
	* @info glwinCreateWindow
	*
	* Creates a Window.
	*
	* @return Window*
	*/
	extern glwin::Window* glwinCreateWindow( const std::string& title, int width, int height, int x, int y, WindowStyle style = WindowStyle::NORMAL, WindowGraphicsAPI graphicsAPI = glwin::WindowGraphicsAPI::OPENGL );

	extern glwin::Window* glwinCreateWindow( WindowCreateInfo& createInfo );

	/*
	* glwinDestroyWindow
	*
	* Destroys a window.
	*
	*/
	extern void glwinDestroyWindow( glwin::Window* window );

	//////////////////////////////////////////////////////////////////////////
	// OPENGL
	//////////////////////////////////////////////////////////////////////////

	/*
	* glwinCreateContext
	*
	* Creates the OpenGL Context.
	*
	*/
	extern void glwinCreateContext( glwin::Window* wind );

	/*
	* glwinDestroyContext
	*
	* Destroys the OpenGL Context.
	*
	*/
	extern void glwinDestroyContext( glwin::Window* wind );

	/*
	* glwinResize
	*
	* Resizes the OpenGL Viewport.
	*
	*/
	extern void glwinResize( glwin::Window* wind, int x, int y, int w, int h );
}