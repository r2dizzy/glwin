/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "glwinTime.h"
#include "Base.h"

#include "Library.h"

namespace glwin {

	void InitTimer( Timer& t )
	{
		uint64_t f;

	#if defined( _WIN32 )
		
		if( QueryPerformanceFrequency( ( LARGE_INTEGER* ) &f ) ) 
			t.Frequency = f;
		else
			t.Frequency = 1000;

	#elif defined ( _POSIX_C_SOURCE  ) && defined ( _GLWIN_USE_POSIX )

		struct sigevent sig;
		struct itimerspec timersepc;

		sig.sigev_notify = SIGEV_SIGNAL;
		sig.sigev_signo = SIG;
		sig.sigev_value.sival_ptr = &t.TimerId;

		timer_create( t.ClockId, &sig, &t.TimerId );

		f = atoll( __argv[ 2 ] );

		timersepc.it_value.tv_sec = f / 1000000000;
		timersepc.it_value.tv_nsec = f % 1000000000;
		timersepc.it_interval.tv_sec = timersepc.it_value.tv_sec;
		timersepc.it_interval.tv_nsec = timersepc.it_value.tv_nsec;

		t.Frequency = f;

		timer_settime( t.TimerId, 0, &timersepc, NULL );

	#endif
	}

	void CloseTimer( Timer& t )
	{
		// For other platforms
	#if defined ( _POSIX_C_SOURCE  ) && defined ( _GLWIN_USE_POSIX )

		timer_delete( t.TimerId );

	#endif
	}

	uint64_t TimerValue( Timer& t )
	{
		uint64_t v;

	#if defined( _WIN32 )

		QueryPerformanceCounter( ( LARGE_INTEGER* )&v );

		return v;

	#elif defined ( _POSIX_C_SOURCE  ) && defined ( _GLWIN_USE_POSIX )

		struct timerspec ts;

		v = ( uint64_t )clock_gettime( t.ClockId, &ts );

		return v;

	#endif
	}

	double GetTime()
	{
		return ( double )( TimerValue( Library::Get().GetTimer() ) - Library::Get().GetTimer().Offset ) / ( Library::Get().GetTimer().Frequency );
	}

	Timer::Timer() : Frequency( 0 ), Offset( 0 )
	{
		InitTimer( *this );
	}

	Timer::~Timer()
	{
		CloseTimer( *this );
	}

}