/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/
#pragma once

#include <string>
#include <signal.h>

#if defined(  _WIN32 ) && !defined( APIENTRY ) && !defined( __CYGWIN__ ) && !defined( __SCITECH_SNAP__ )
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <Windows.h>
#undef APIENTRYP
#endif // _WIN32 && !APIENTRY && !__CYGWIN__ && !__SCITECH_SNAP__

#if defined( __linux__ )
#include <xcb/xcb.h>
#endif // __linux__

#if !defined( APIENTRY )
#define APIENTRY
#endif

#if !defined( APIENTRYP )
#define APIENTRYP APIENTRY *
#endif

#define _GLW_ASSERT( x ) if( !(x) ) { glwin::BreakDebug(); }
#define _GLW_CLASS_NAME L"GLWIN01"

#define _GLW_NAME_OF( x ) #x
#define _GLW_MOUSE_BUTTONS 6

#if defined ( __linux__ )
#define _GLW_TO_RESPONSE( x, num ) x->response_type & ~num
#endif

#define _GLW_IS_WINDOW_NULL( x ) /*if*/( x->Library().Handle == NULL )

// Platform macros
#if defined( _WIN32 )
#define _free_library( x ) FreeLibrary( x )
#define _load_library( x ) LoadLibrary( TEXT( x ) )
#define _library_func( x, y ) GetProcAddress( x, y )
#else
#define _free_library( x ) dlclose( x )
#define _load_library( x, y ) dlopen( x, y )
#define _library_func( x, y ) dlysm( x, y )
#endif

namespace glwin {

	inline void BreakDebug() 
	{
	#if defined( _WIN32 )
		__debugbreak();
	#elif defined ( __linux__ )
		raise( SIGTRAP );
	#endif
		return;
	}

	// https://stackoverflow.com/a/27296/12994956
	inline std::wstring StringToWString( const std::string& s )
	{
	#if defined( _WIN32 )
		int len;
		int slength = ( int )s.length() + 1;
		len = MultiByteToWideChar( 0, 0, s.c_str(), slength, 0, 0 );

		wchar_t* buf = new wchar_t[ len ];
		MultiByteToWideChar( 0, 0, s.c_str(), slength, buf, len );
		std::wstring r( buf );
		delete[] buf;
		return r;
	#else
		return L"";
	#endif
	}
}

// For macros below they should all be _GLWIN_*_***

#define _GLWIN_A_KEY  1
#define _GLWIN_B_KEY  2
#define _GLWIN_C_KEY  3
#define _GLWIN_D_KEY  4
#define _GLWIN_E_KEY  5
#define _GLWIN_F_KEY  6
#define _GLWIN_G_KEY  7
#define _GLWIN_H_KEY  8
#define _GLWIN_I_KEY  9
#define _GLWIN_J_KEY 10
#define _GLWIN_K_KEY 11
#define _GLWIN_L_KEY 12
#define _GLWIN_M_KEY 13
#define _GLWIN_N_KEY 14
#define _GLWIN_O_KEY 15
#define _GLWIN_P_KEY 16
#define _GLWIN_Q_KEY 17
#define _GLWIN_R_KEY 18
#define _GLWIN_S_KEY 19
#define _GLWIN_T_KEY 20
#define _GLWIN_U_KEY 21
#define _GLWIN_W_KEY 22
#define _GLWIN_X_KEY 23
#define _GLWIN_Y_KEY 24
#define _GLWIN_Z_KEY 25

#define _GLWIN_TAB_KEY     40
#define _GLWIN_CAPS_KEY    41
#define _GLWIN_SHIFT_KEY   42
#define _GLWIN_L_CTRL_KEY  43
#define _GLWIN_UNIQUE_KEY  45 // "Windows" Key
#define _GLWIN_L_ALT_KEY   46
#define _GLWIN_ESC_KEY     47
#define _GLWIN_INS_KEY     48

#define _GLWIN_BLACKSLASH_KEY 49
#define _GLWIN_FORWARDSLASH_KEY 50

#define _GLWIN_F1_KEY   51
#define _GLWIN_F2_KEY   52
#define _GLWIN_F3_KEY   53
#define _GLWIN_F4_KEY   54
#define _GLWIN_F5_KEY   55
#define _GLWIN_F6_KEY   56
#define _GLWIN_F7_KEY   57
#define _GLWIN_F8_KEY   58
#define _GLWIN_F9_KEY   59
#define _GLWIN_F10_KEY  60
#define _GLWIN_F11_KEY  61
#define _GLWIN_F12_KEY  62

#define _GLWIN_PRINT_SC_KEY   63
#define _GLWIN_SCR_IK_KEY     64
#define _GLWIN_PAUSE_BRK_KEY  65

#define _GLWIN_HOME_KEY 66
#define _GLWIN_PG_KEY   67 // "Page Up" Key
#define _GLWIN_PD_KEY   68 // "Page Down" Key
#define _GLWIN_DEL_KEY  69 // "Delete" Key
#define _GLWIN_END_KEY  70

#define _GLWIN_NUM_IK_KEY              71
#define _GLWIN_FORWARDSLASH_NUMPAD_KEY 72
#define _GLWIN_STAR_NUMPAD_KEY         73
#define _GLWIN_DASH_NUMPAD_KEY         74 // "Hyphen" Key
#define _GLWIN_HOME_NUMPAD_KEY         75 // "7" Key as well
#define _GLWIN_NUMPAD_UP_KEY           76
#define _GLWIN_NUMPAD_9_KEY            77
#define _GLWIN_NUMPAD_4_KEY            78
#define _GLWIN_NUMPAD_5_KEY            79
#define _GLWIN_NUMPAD_6_KEY            80
#define _GLWIN_NUMPAD_1_KEY            81
#define _GLWIN_NUMPAD_2_KEY            82
#define _GLWIN_NUMPAD_3_KEY            83
#define _GLWIN_PLUS_NUMPAD_KEY         84
#define _GLWIN_ENTER_NUMPAD_KEY        85
#define _GLWIN_DEL_NUMPAD_KEY          86
#define _GLWIN_INS_NUMPAD_KEY          87

#define _GLWIN_ACUTE_KEY 88

#define _GLWIN_UP_KEY    89 // "Up" Arrow key
#define _GLWIN_DOWN_KEY  90 // "Down" Arrow key
#define _GLWIN_LEFT_KEY  91 // "Left" Arrow key
#define _GLWIN_RIGHT_KEY 92 // "Right" Arrow key

#define _GLWIN_1_KEY 101
#define _GLWIN_2_KEY 102
#define _GLWIN_3_KEY 103
#define _GLWIN_4_KEY 104
#define _GLWIN_5_KEY 105
#define _GLWIN_6_KEY 106
#define _GLWIN_7_KEY 107
#define _GLWIN_8_KEY 108
#define _GLWIN_9_KEY 109
#define _GLWIN_0_KEY 110

#define _GLWIN_DASH_KEY   111
#define _GLWIN_EQUALS_KEY 211

#define _GLWIN_ENTER_KEY 112
#define _GLWIN_BACKSPACE_KEY 113

#define _GLWIN_R_SHIFT_KEY 114
#define _GLWIN_R_CTRL_KEY 115
#define _GLWIN_MENU_KEY 116 // I have no idea what this key is called but its right under the right shift key
#define _GLWIN_FN_KEY 117
#define _GLWIN_R_ALT_KEY 118


#define _GLWIN_HASHTAG_KEY 120
#define _GLWIN_AT_KEY 121
#define _GLWIN_SEMICOLON_ALT_KEY 122
#define _GLWIN_APOSTROPHE_KEY 123
#define _GLWIN_PERIOD_KEY 124
#define _GLWIN_COMMA_KEY 125
#define _GLWIN_OPEN_BRACKET_KEY 126
#define _GLWIN_CLOSE_BRACKET_KEY 127

#define _GLWIN_V_KEY 130
#define _GLWIN_SPC_KEY 131

#define _GLWIN_M_LEFT_DOWN 140 
#define _GLWIN_M_LEFT_UP 145

#define _GLWIN_M_RIGHT_UP 150
#define _GLWIN_M_RIGHT_DOWN 155

#define _GLWIN_M_MIDDLE_UP 160
#define _GLWIN_M_MIDDLE_DOWN 165

#define _GLWIN_M_OTHER1_UP   170
#define _GLWIN_M_OTHER1_DOWN 175
#define _GLWIN_M_OTHER2_UP   180
#define _GLWIN_M_OTHER2_DOWN 185
#define _GLWIN_M_OTHER3_UP   190
#define _GLWIN_M_OTHER3_DOWN 195


#define _GLWIN_M_LEFT   141
#define _GLWIN_M_RIGHT  151
#define _GLWIN_M_MIDDLE 161
#define _GLWIN_M_OTHER1 171
#define _GLWIN_M_OTHER2 181
#define _GLWIN_M_OTHER3 191

#define _GLWIN_NUMPAD_7_KEY            200
#define _GLWIN_NUMPAD_8_KEY            201