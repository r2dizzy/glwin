/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#pragma once

#include <thread>

namespace glwin {

	// Wrapper for the std thread
	class Thread
	{
	public:
		// From the msvc lmao
		template <class Func, class... Args, std::enable_if_t<!std::is_same_v<std::_Remove_cvref_t<Func>, Thread>, int> = 0>
		Thread( const char name, Func&& func, Args&&... args )
		{
			m_Name = name;

			m_StdThread( std::forward<Func>( func ), std::forward<Args>( args )... );
		}

		Thread() : m_StdThread{} {}

		~Thread() 
		{
			if( m_StdThread.joinable() ) 
			{
				m_StdThread.detach();
				m_StdThread.join();
			}
		}

		void Join() { m_StdThread.join(); }
		void Detach() { m_StdThread.detach(); }

	private:

		const char* m_Name = "";

		std::thread m_StdThread;

	};
}