// dear imgui: Platform Backend for Glwin

#include "imgui.h"
#include "imgui_impl_glwin.h"

// GLWIN includes

#include <glwin/Creators.h>
#include <glwin/Window.h>
#include <glwin/Base.h>

#include <windows.h>
#include <tchar.h>
#include <dwmapi.h>

// CHANGELOG
// 25/8/21 Created imgui_impl_glwin
// 4/1/22  Change branch to imgui-ffx to master
// 5/1/22  Add Vulkan to glwinClientApi
// 22/1/22 Add to glwin public 

// GLFW data
enum GlwinClientApi
{
	GlwinClientApi_Unknown,
	GlwinClientApi_OpenGL,
	GlwinClientApi_Vulkan
};

struct ImGui_ImplGlwin_Data
{
	glwin::Window*          Window;
	glwin::Window*          MouseWindow;
	GlwinClientApi          ClientApi;
	double                  Time;
	bool                    MouseJustPressed[ ImGuiMouseButton_COUNT ];

	ImGui_ImplGlwin_Data() { memset( this, 0, sizeof( *this ) ); }
};

static ImGui_ImplGlwin_Data* ImGui_ImplGlwin_GetBackendData()
{
	return ImGui::GetCurrentContext() ? ( ImGui_ImplGlwin_Data* )ImGui::GetIO().BackendPlatformUserData : NULL;
}

static const char* ImGui_ImplGlwin_GetClipboardText( void* user_data )
{
	return "";
}

static void ImGui_ImplGlwin_SetClipboardText( void* user_data, const char* text )
{
}

static void ImGui_ImplGlwin_MouseUpCB( int code )
{
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();

	if ( code != -1 || code != 0 && code < IM_ARRAYSIZE( bd->MouseJustPressed ) )
		bd->MouseJustPressed[ code ] = false;
}

static void ImGui_ImplGlwin_MouseDownCB( int code )
{
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();

	if( code != -1 || code != 0 && code < IM_ARRAYSIZE( bd->MouseJustPressed ) )
		bd->MouseJustPressed[ code ] = true;
}

static bool ImGui_ImplGlwin_Init( glwin::Window* window, GlwinClientApi client_api )
{
	ImGuiIO& io = ImGui::GetIO();
	IM_ASSERT( io.BackendPlatformUserData == NULL && "Already initialized a platform backend!" );

	// Setup backend capabilities flags
	ImGui_ImplGlwin_Data* bd = IM_NEW( ImGui_ImplGlwin_Data )( );
	io.BackendPlatformUserData = ( void* )bd;
	io.BackendPlatformName = "imgui_impl_glwin";
	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;         // We can honor GetMouseCursor() values (optional)
	io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;          // We can honor io.WantSetMousePos requests (optional, rarely used)

	bd->Window = window;
	bd->Time = 0.0;

	// Keyboard mapping. Dear ImGui will use those indices to peek into the io.KeysDown[] array.
	io.KeyMap[ ImGuiKey_Tab ]         = _GLWIN_TAB_KEY;
	io.KeyMap[ ImGuiKey_LeftArrow ]   = _GLWIN_LEFT_KEY;
	io.KeyMap[ ImGuiKey_RightArrow ]  = _GLWIN_RIGHT_KEY;
	io.KeyMap[ ImGuiKey_UpArrow ]     = _GLWIN_UP_KEY;
	io.KeyMap[ ImGuiKey_DownArrow ]   = _GLWIN_DOWN_KEY;
	io.KeyMap[ ImGuiKey_PageUp ]      = _GLWIN_PG_KEY;
	io.KeyMap[ ImGuiKey_PageDown ]    = _GLWIN_PD_KEY;
	io.KeyMap[ ImGuiKey_Home ]        = _GLWIN_HOME_KEY;
	io.KeyMap[ ImGuiKey_End ]         = _GLWIN_END_KEY;
	io.KeyMap[ ImGuiKey_Insert ]      = _GLWIN_INS_KEY;
	io.KeyMap[ ImGuiKey_Delete ]      = _GLWIN_DEL_KEY;
	io.KeyMap[ ImGuiKey_Backspace ]   = _GLWIN_BACKSPACE_KEY;
	io.KeyMap[ ImGuiKey_Space ]       = _GLWIN_SPC_KEY;
	io.KeyMap[ ImGuiKey_Enter ]       = _GLWIN_ENTER_KEY;
	io.KeyMap[ ImGuiKey_Escape ]      = _GLWIN_ESC_KEY;
	io.KeyMap[ ImGuiKey_KeyPadEnter ] = _GLWIN_ENTER_NUMPAD_KEY;
	io.KeyMap[ ImGuiKey_A ]           = _GLWIN_A_KEY;
	io.KeyMap[ ImGuiKey_C ]           = _GLWIN_C_KEY;
	io.KeyMap[ ImGuiKey_V ]           = _GLWIN_V_KEY;
	io.KeyMap[ ImGuiKey_X ]           = _GLWIN_X_KEY;
	io.KeyMap[ ImGuiKey_Y ]           = _GLWIN_Y_KEY;
	io.KeyMap[ ImGuiKey_Z ]           = _GLWIN_Z_KEY;

	io.SetClipboardTextFn = ImGui_ImplGlwin_SetClipboardText;
	io.GetClipboardTextFn = ImGui_ImplGlwin_GetClipboardText;
	io.ClipboardUserData = bd->Window;


#if defined( _WIN32 )
	io.ImeWindowHandle = ( void* )( bd->Window->NativeWindow() );
#endif // _WIN32

	bd->ClientApi = client_api;

	glwin::SetMouseUpCallback   ( bd->Window, ImGui_ImplGlwin_MouseUpCB );
	glwin::SetMouseDownCallback ( bd->Window, ImGui_ImplGlwin_MouseDownCB );

	return true;

}

bool ImGui_ImplGlwin_InitForOpenGL( void* window )
{
	return ImGui_ImplGlwin_Init( ( glwin::Window* )window, GlwinClientApi_OpenGL );
}

void ImGui_ImplGlwin_Shutdown()
{
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();
	IM_ASSERT( bd != NULL && "No platform backend to shutdown, or already shutdown?" );
	ImGuiIO& io = ImGui::GetIO();

	io.BackendPlatformName = NULL;
	io.BackendPlatformUserData = NULL;
	IM_DELETE( bd );
}

static void ImGui_ImplGlwin_UpdateMousePosAndButtons()
{
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();
	ImGuiIO& io = ImGui::GetIO();

	const ImVec2 mouse_pos_prev = io.MousePos;
	io.MousePos = ImVec2( -FLT_MAX, -FLT_MAX );
	
	if ( bd->Window->MouseButtonState( _GLWIN_M_LEFT ) )
	{
		io.MouseDown[ 0 ] = true;
	}
	else if( bd->Window->MouseButtonState( _GLWIN_M_RIGHT ) )
	{
		io.MouseDown[ 1 ] = true;

	}
	else if( bd->Window->MouseButtonState( _GLWIN_M_MIDDLE ) )
	{
		io.MouseDown[ 2 ] = true;

	} 
	else if( bd->Window->MouseButtonState( _GLWIN_M_OTHER1 ) || bd->Window->MouseButtonState( _GLWIN_M_OTHER2 ) || bd->Window->MouseButtonState( _GLWIN_M_OTHER3 ) )
	{
		io.MouseDown[ 3 ] = true;
		io.MouseDown[ 4 ] = true;
		io.MouseDown[ 5 ] = true;
	}
	else
	{
		for( int i : io.MouseDown )
		{
			io.MouseDown[ i ] = false;
		}
	}

	const bool fcd = bd->Window->GetWindowAttribute( _GLW_WINDOW_FOCUSED );

	glwin::Window* mouse_window = ( bd->MouseWindow == bd->Window || fcd ) ? bd->Window : NULL;

	if( io.WantSetMousePos && fcd )
		bd->Window->SetMousePos( mouse_pos_prev.x, mouse_pos_prev.y );

	if( mouse_window ) 
	{
		int x, y;
		bd->Window->GetMousePos( &x, &y );
		io.MousePos = ImVec2( x, y );
	}
}

static void ImGui_ImplGlwin_UpdateMouseCursor() 
{
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();
	ImGuiIO& io = ImGui::GetIO();

	if( ( io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange ) )
		return;

	ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
	if( imgui_cursor == ImGuiMouseCursor_None || io.MouseDrawCursor )
	{
		bd->Window->HideMouse();
	}
	else
	{
		bd->Window->NormalMouse();
	}
}

void ImGui_ImplGlwin_NewFrame()
{
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplGlwin_Data* bd = ImGui_ImplGlwin_GetBackendData();
	IM_ASSERT( bd != NULL && "Did you call ImGui_ImplGlfw_InitForXXX()?" );

	// Setup display size (every frame to accommodate for window resizing)
	int w, h;
	int display_w, display_h;

	w = bd->Window->Width();
	h = bd->Window->Height();

	display_w = bd->Window->GetFramebufferSize().first;
	display_h = bd->Window->GetFramebufferSize().second;

	io.DisplaySize = ImVec2( ( float )w, ( float )h );
	if( w > 0 && h > 0 )
		io.DisplayFramebufferScale = ImVec2( ( float )display_w / w, ( float )display_h / h );

	// Setup time step
	double current_time = glwin::GetTime();
	io.DeltaTime = bd->Time > 0.0 ? ( float )( current_time - bd->Time ) : ( float )( 1.0f / 60.0f );
	bd->Time = current_time;

	ImGui_ImplGlwin_UpdateMousePosAndButtons();
	ImGui_ImplGlwin_UpdateMouseCursor();

}

//---------------------------------------------------------------------------------------------------------