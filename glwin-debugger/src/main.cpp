/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "glwin/Window.h"
#include "glwin/Creators.h"
#include "glwin/Library.h"

#include <string>
#include <vector>

void KeyDownCB( int a )
{
	printf( "%i", a );

	if ( a == _GLWIN_W_KEY )
	{
		printf( "W down" );
	}
}

void KeyReleaseCB( int a )
{
	printf( "%i", a );
}

void MouseKey( int a )
{
	printf( "Mouse Up : code %i\n", a );
}

void MouseKeyDown( int a )
{
	printf( "Mouse Down : code %i\n", a );
}

void MouseMoved( float& a, float& b )
{
	printf( "Mouse Moved : code %f %f\n", a, b );
}

int main()
{	
	glwinc::WindowCreateInfo windowCreateInfo{};
	windowCreateInfo.GraphicsAPI = glwin::WindowGraphicsAPI::OPENGL;
	windowCreateInfo.Width  = 1280;
	windowCreateInfo.Height = 720;
	windowCreateInfo.Title  = "glwin Window";
	windowCreateInfo.Style  = glwin::WindowStyle::NORMAL;

	glwin::Window* wind = glwinc::glwinCreateWindow( windowCreateInfo );

	glwin::SetMouseDownCallback( wind, MouseKeyDown );
	glwin::SetMouseUpCallback( wind, MouseKey );
	glwin::SetMouseMovedCallback( wind, MouseMoved );

	glwin::SetKeyReleaseCallback( wind, KeyReleaseCB );
	glwin::SetKeyDownCallback( wind, KeyDownCB );

	glwinc::glwinCreateContext( wind );

	while( !wind->IsWindowPendingClose() )
	{
		wind->Update();

		wind->SwapBuffers();
	}

	wind->Disconnect();
	glwinc::glwinDestroyWindow( wind );
	glwin::Shutdown();

	return EXIT_SUCCESS;
}