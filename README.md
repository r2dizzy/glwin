# glwin
### About
Glwin *(written as glwin)*, is a cross platform windowing library in C++ 20 / C++ 17. <br>
Designed to be fast and simple to easy use with minial includes. In full C++ and no dependencies!

## Building

### Windows

To build glwin on windows *(note only works on windows 10 & win64)*, you will need [Premake](https://premake.github.io/download). <br>
Use the `vs2019` command or whatever IDE you are using to create the project files.
Once created open the `.sln` file / what ever your workspace files are, then build the "glwin" project. You can build the full workspace if you want to. <br>

<br>

If you are going to use glwin in a project you only need to build the "glwin" project, as any others such as "glwin-debugger" is only for an example.

<br>

If you are using the [imgui](https://github.com/BEASTSM96/glwin/tree/imgui-ffx) branch, the same step should follow.

### Linux

#### Linux Packages <br>

`libx11-dev libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev libglu1-mesa-dev` <br>

To build glwin on linux *(note only tested on ubuntu 21.04 & 21.10)*, you will need [Premake](https://premake.github.io/download). <br>
Use the `codelite` command or whatever IDE you are using to create the project files.
Once created open the `.workspace` file / what ever your workspace files are, then build the "glwin" project. You can build the full workspace if you want to. <br>

<br>

If you are going to use glwin in a project you only need to build the "glwin" project, as any others such as "glwin-debugger" is only for an example.

<br>

If you are using the [imgui](https://github.com/BEASTSM96/glwin/tree/imgui-ffx) branch, the same step should follow.

### MacOS

MacOS is not supported right now.

## Wiki 

[Read More](https://github.com/BEASTSM96/glwin/wiki)

## Window Library Info

For Linux we use [xcb](https://xcb.freedesktop.org/). <br>
For Windows we use the [win32 api](https://docs.microsoft.com/en-us/windows/win32/api/). <br>

## Additional Linux packages

`libx11-xcb-dev`

