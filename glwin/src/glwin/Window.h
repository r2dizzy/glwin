/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#pragma once

#include "Base.h"
#include "WindowCallbacks.h"

#if defined( __linux__ )
#include <xcb/xcb.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/gl.h>
#include <GL/glx.h>
#endif

#include <string>
#include <cstring>
#include <unordered_map>

#define _GLW_WINDOW_CLOSE          1
#define _GLW_WINDOW_ICONIC         2 // If minimised
#define _GLW_WINDOW_FOCUSED        3
#define _GLW_WINDOW_MOUSE_HOVERED  4
#define _GLW_WINDOW_MOUSE_CAPTURED 5
// Settings
#define _GLW_WIN_SETTING_BORDER 100

namespace glwin {

	enum class WindowStyle
	{
		NORMAL,
		EMPTY, // "Undecorated" window
		THIN_BORDER,
		HIDE,
		ICONIC
	};

	enum class WindowGraphicsAPI
	{
		NONE,
		OPENGL,

	#if defined( _WIN32 )
		DIRECTX_11,
		DIRECTX_12,
	#endif

		VULKAN
	};

	typedef const uint8_t* ( APIENTRYP _PFNGLGETSTRINGPROC ) ( unsigned int name );
	typedef void ( APIENTRYP _PFNGLVIEWPORTPROC ) ( int x, int y, int width, int height );
	typedef void ( APIENTRYP _PFNGLCLEARPROC ) ( unsigned int mask );
	typedef void ( APIENTRYP _PFNGLCLEARCOLORPROC ) ( float r, float g, float b, float a );

	class BaseLibrary 
	{
	public:
		BaseLibrary() 
		{
			MouseButtons[ 0 ] = _GLWIN_M_LEFT;
			MouseButtons[ 1 ] = _GLWIN_M_RIGHT;
			MouseButtons[ 2 ] = _GLWIN_M_MIDDLE;
			MouseButtons[ 3 ] = _GLWIN_M_OTHER1;
			MouseButtons[ 4 ] = _GLWIN_M_OTHER2;
			MouseButtons[ 5 ] = _GLWIN_M_OTHER3;
		}

		BaseLibrary& operator =( const BaseLibrary& other )
		{
			if( this == &other )
				return *this;

			std::copy( std::begin( other.MouseButtons ), std::end( other.MouseButtons ), MouseButtons );
		}

		BaseLibrary( const BaseLibrary& other ) 
		{
			std::copy( std::begin( other.MouseButtons ), std::end( other.MouseButtons ), MouseButtons );
		}

		~BaseLibrary()
		{

		}

	public:
		// TODO: Support other rendering api

		// The current mouse buttons down - 140 L, 150 - R, 160 - M
		int MouseDown = -1;
		int MouseButtons[ _GLW_MOUSE_BUTTONS ];

		bool ShouldLoadGraphicsAPI = true;
		WindowGraphicsAPI GraphicsAPI;

		// Loaded GL Func

		_PFNGLCLEARPROC ClearFunc = NULL;
		_PFNGLCLEARCOLORPROC ClearColorFunc = NULL;
		_PFNGLVIEWPORTPROC ViewportFunc = NULL;
		_PFNGLGETSTRINGPROC GetStringFunc = NULL;
	};

#if defined( _WIN32 )

	class Windows32Library : public BaseLibrary
	{
	public:
		Windows32Library()
		{
			Instance = GetModuleHandleW( NULL );
			Handle = NULL;
			RenderContext = NULL;
			DC = NULL;
			GLInstance = NULL;
			CurrentCursor = NULL;

			MouseButtons[ 0 ] = _GLWIN_M_LEFT;
			MouseButtons[ 1 ] = _GLWIN_M_RIGHT;
			MouseButtons[ 2 ] = _GLWIN_M_MIDDLE;
			MouseButtons[ 3 ] = _GLWIN_M_OTHER1;
			MouseButtons[ 4 ] = _GLWIN_M_OTHER2;
			MouseButtons[ 5 ] = _GLWIN_M_OTHER3;

			Cursors.insert( { "IDC_ARROW", LoadCursorW( Instance, IDC_ARROW )             } );
			Cursors.insert( { "IDC_APPSTARTING", LoadCursorW( Instance, IDC_APPSTARTING ) } );
			Cursors.insert( { "IDC_CROSS", LoadCursorW( Instance, IDC_CROSS )             } );
			Cursors.insert( { "IDC_HELP", LoadCursorW( Instance, IDC_HELP )               } );
			Cursors.insert( { "IDC_ICON", LoadCursorW( Instance, IDC_ICON )               } );
		}

		Windows32Library& operator =( const Windows32Library& other )
		{
			if( this == &other )
				return *this;

			Instance = GetModuleHandleW( NULL );
			Handle = other.Handle;
			RenderContext = other.RenderContext;
			DC = other.DC;
			GLInstance = other.GLInstance;
			CurrentCursor = other.CurrentCursor;

			std::copy( std::begin( other.MouseButtons ), std::end( other.MouseButtons ), MouseButtons );

			Cursors = other.Cursors;

			return *this;
		}

		Windows32Library( const Windows32Library& other )
		{
			Instance = GetModuleHandleW( NULL );
			Handle = other.Handle;
			RenderContext = other.RenderContext;
			DC = other.DC;
			GLInstance = other.GLInstance;
			CurrentCursor = other.CurrentCursor;

			std::copy( std::begin( other.MouseButtons ), std::end( other.MouseButtons ), MouseButtons );

			Cursors = other.Cursors;
		}

		~Windows32Library() 
		{
			if( Handle )
				DestroyWindow( Handle );

			if( Instance )
				Instance = NULL;
		}

	public:

		// TODO: Support other rendering api

		HWND Handle;
		HINSTANCE Instance;
		HGLRC RenderContext;
		HDC DC;

		std::unordered_map<const char*, HCURSOR> Cursors;
		
		HCURSOR CurrentCursor = NULL;
		HINSTANCE GLInstance = NULL;

		void SwapWin32Keycodes( int& code ) 
		{
		}

		int Windows32ToGLWin( WindowStyle style )
		{
			switch( style )
			{
				case WindowStyle::NORMAL:
					return WS_OVERLAPPEDWINDOW;

				case WindowStyle::EMPTY:
					return WS_OVERLAPPEDWINDOW; // Cannot receive input from the user

				case WindowStyle::THIN_BORDER:
					return WS_BORDER;

				case WindowStyle::HIDE:
					return WS_MINIMIZE; // Lmao well this just minimizes the window

				case WindowStyle::ICONIC:
					return WS_MINIMIZE;
			}

			// WindowStyle::NORMAL
			return WS_OVERLAPPEDWINDOW;
		}
	};

#elif defined( __linux__ )

	class LinuxXCBLibrary : public BaseLibrary
	{
	public:
		LinuxXCBLibrary()
		{
		}

		~LinuxXCBLibrary()
		{
		}

	public:

		xcb_connection_t*         Connection        = NULL;
		xcb_atom_t*               Atom              = NULL;
		xcb_atom_t*               Atom_Protocals    = NULL;
		xcb_atom_t*               Atom_CloseWindow  = NULL;
		xcb_intern_atom_cookie_t* AtomCookie        = NULL;
		xcb_screen_t*             Screen            = NULL;
		xcb_window_t              Handle            = NULL;
		int                       VisualID          = NULL;
		GLXDrawable               Drawable          = NULL;
		GLXWindow                 glxWindow         = NULL;
		GLXContext                glxContext        = NULL;
		xcb_colormap_t            ColorMap;
		Display*                  xDisplay;
		int                       DefaultScreen     = 0;

		int VisualAttribs[ 23 ] =
		{
			GLX_X_RENDERABLE, 
			True,
			GLX_DRAWABLE_TYPE,
			GLX_WINDOW_BIT,
			GLX_RENDER_TYPE, 
			GLX_RGBA_BIT,
			GLX_X_VISUAL_TYPE, 
			GLX_TRUE_COLOR,
			GLX_RED_SIZE, 
			8,
			GLX_GREEN_SIZE, 
			8,
			GLX_BLUE_SIZE, 
			8,
			GLX_ALPHA_SIZE, 
			8,
			GLX_DEPTH_SIZE, 
			24,
			GLX_STENCIL_SIZE, 
			8,
			GLX_DOUBLEBUFFER, 
			True,
			//GLX_SAMPLE_BUFFERS  , 1,
			//GLX_SAMPLES         , 4,
			None
		};

		int LinuxStyleToGLWin( WindowStyle style )
		{
			switch( style )
			{
			}
		}
	};

#endif

	class Window
	{
	public:
		Window() = default;

		// NULL_WINDOW
		Window( bool null_win = true ) 
		#if defined( _WIN32 )
			: m_Handle( {} ), m_Callbacks( {} ), m_Title( "" ), m_Width( 0 ), m_Height( 0 ), m_OldKeyStates(), m_KeyStates(), m_Attributes(), m_WindowProc( NULL )
		#elif defined( __linux__ )
			: m_Handle( {} ), m_Callbacks( {} ), m_Title( "" ), m_Width( 0 ), m_Height( 0 ), m_OldKeyStates(), m_KeyStates(), m_Attributes()
		#endif
		{
		}

		Window( std::string n, int w, int h );

		~Window() { m_Title = ""; m_Width = 0; m_Height = 0; }

		void Update();

		//////////////////////////////////////////////////////////////////////////
		// HELPER FUNCS
		//////////////////////////////////////////////////////////////////////////

	public:

		void CreateWindowInfo();
		void CreateWindowProc();
		
		void SetWindowAttribute( int attrib, bool val ) 
		{
			_GLW_ASSERT( m_Attributes.find( attrib ) != m_Attributes.end() ); // Could not find the attribute int the map

			m_Attributes[ attrib ] = val;
		}

		bool GetWindowAttribute( int attrib )
		{
			_GLW_ASSERT( m_Attributes.find( attrib ) != m_Attributes.end() ); // Could not find the attribute int the map

			return m_Attributes[ attrib ];
		}

		bool IsWindowPendingClose()
		{
			if( GetWindowAttribute( _GLW_WINDOW_CLOSE ) )
				return true;

			return false;
		}

		int& Width() { return m_Width; }
		int& Height() { return m_Height; }

		void SetHeight( int h ) { m_Height = h; }
		void SetWidth( int w ) { m_Width = w; }

		void GetFramebufferSize( int* x, int* y );

		// Linux only.
		void Disconnect();

		//////////////////////////////////////////////////////////////////////////
		// Handles & Callbacks
		//////////////////////////////////////////////////////////////////////////
	public:

	#if defined( _WIN32 )
		Windows32Library m_Handle;
	#elif defined( __linux__ )
		LinuxXCBLibrary m_Handle;
	#endif

		WindowCallbacks m_Callbacks;

	#if defined( _WIN32 )
	
		HWND& NativeWindow() { return m_Handle.Handle; }
		const HWND& NativeWindow() const { return m_Handle.Handle; }

		Windows32Library& Library() { return m_Handle; }
		const Windows32Library& Library() const { return m_Handle; }

	#elif defined( __linux__ )

		LinuxXCBLibrary& Library() { return m_Handle; }
		const LinuxXCBLibrary& Library() const { return m_Handle; }

	#endif

		//////////////////////////////////////////////////////////////////////////
		// HANDLE INPUT
		//////////////////////////////////////////////////////////////////////////

		void HandleWindowInput( char key );

		void GetMousePos( int* x, int* y );

		void SetMousePos( int x, int y );

		void CaptureMouse();
		void UncaptureMouse();

		/////////////////////////////////////
		// MOUSE FUCS
		/////////////////////////////////////

		void HideMouse();
		void ShowMouse();
		void NormalMouse();

		void SetMouseDown( int button );
		int CurrentMouseButtonDown() { return Library().MouseDown; }
		bool MouseButtonState( int button ) { return Library().MouseDown == button; }
		bool MouseDown() { return Library().MouseDown != -1; }

		void SwapBuffers();

	private:

		void CreateAttributes() 
		{
			_GLW_ASSERT( m_Attributes.find( 1 ) == m_Attributes.end() ); // _GLW_WINDOW_CLOSE in already in the map
			_GLW_ASSERT( m_Attributes.find( 2 ) == m_Attributes.end() ); // _GLW_WINDOW_ICONIC in already in the map
			_GLW_ASSERT( m_Attributes.find( 3 ) == m_Attributes.end() ); // _GLW_WINDOW_FOCUSED in already in the map
			_GLW_ASSERT( m_Attributes.find( 4 ) == m_Attributes.end() ); // _GLW_WINDOW_MOUSE_HOVERED in already in the map
			_GLW_ASSERT( m_Attributes.find( 5 ) == m_Attributes.end() ); // _GLW_WINDOW_MOUSE_CAPTURED in already in the map


			m_Attributes.insert( { _GLW_WINDOW_ICONIC, false } );
			m_Attributes.insert( { _GLW_WINDOW_CLOSE, false } );
			m_Attributes.insert( { _GLW_WINDOW_FOCUSED, false } );
			m_Attributes.insert( { _GLW_WINDOW_MOUSE_HOVERED, false } );
			m_Attributes.insert( { _GLW_WINDOW_MOUSE_CAPTURED, false } );
		}

		void UpdateInput();

	private:

	#if defined( _WIN32 )

		WNDPROC m_WindowProc;

	#endif

		std::string m_Title = "";
		std::unordered_map<int, bool> m_Attributes;
		int m_Width = 0;
		int m_Height = 0;

		unsigned char m_KeyStates[ 256 ];
		unsigned char m_OldKeyStates[ 256 ];
	};

	// Set Callbacks

	inline void SetCloseCallback( Window* win, WINDOWCLOSEPROC closeCallback )
	{
		win->m_Callbacks.CloseCallback = closeCallback;
	}

	inline void SetResizeCallback( Window* win, WINDOWRESIZEPROC resizeCallback )
	{
		win->m_Callbacks.ResizeCallback = resizeCallback;
	}

	inline void SetDragCallback( Window* win, WINDOWDRAGPROC dragCallback )
	{
		win->m_Callbacks.DragCallback = dragCallback;
	}

	inline void SetMaximizeCallback( Window* win, WINDOWMAXPROC maximizeCallback )
	{
		win->m_Callbacks.MaximizeCallback = maximizeCallback;
	}

	inline void SetMinimizeCallback( Window* win, WINDOWICONIFYPROC minimizeCallback )
	{
		win->m_Callbacks.MinimizeCallback = minimizeCallback;
	}

	inline void SetKeyDownCallback( Window* win, WINDOWKEYDOWN cb )
	{
		win->m_Callbacks.KeyDownCallback = cb;
	}

	inline void SetKeyReleaseCallback( Window* win, WINDOWKEYRELEASE cb )
	{
		win->m_Callbacks.KeyReleaseCallback = cb;
	}

	inline void SetMouseDownCallback( Window* win, WINDOWMOUSEDOWN cb )
	{
		win->m_Callbacks.MouseDownCallback = cb;
	}

	inline void SetMouseUpCallback( Window* win, WINDOWMOUSEUP cb )
	{
		win->m_Callbacks.MouseUpCallback = cb;
	}

	inline void SetMouseMovedCallback( Window* win, WINDOWMOUSEMOVED cb )
	{
		win->m_Callbacks.MouseMovedCallback = cb;
	}
}