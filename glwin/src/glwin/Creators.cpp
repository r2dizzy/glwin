/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "Creators.h"
#include "KeyHelpers.h"

#if defined( _WIN32 )
#include <windowsx.h> // GET_X_LPARAM, GET_Y_LPARAM
#elif defined ( __linux__ )
#include <dlfcn.h>
#include <X11/Xlib-xcb.h>
#endif

namespace glwinc {

	using namespace glwin;

#if defined ( _WIN32 )

	LRESULT CALLBACK WindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
	{
		Window* wind = ( Window* )GetPropW( hwnd, L"GLWIN" );

		if( wind )
		{
			switch( uMsg )
			{
				case WM_CREATE:
				{
					SetWindowLongPtr( hwnd, GWLP_USERDATA, ( LONG_PTR )GetPropW( hwnd, L"GLWIN" ) );
					SetWindowLong( hwnd, GWL_STYLE, GetWindowLong( hwnd, GWL_STYLE ) | WS_CAPTION | WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX );
				} break;

				case WM_ENTERSIZEMOVE:
				{
					UINT width = LOWORD( lParam ); //-V1037
					UINT height = HIWORD( lParam );

					glwinResize( wind, 0, 0, width, height );

					if( wind->m_Callbacks.ResizeCallback )
						wind->m_Callbacks.ResizeCallback( wind->Width(), wind->Height() );
				} break;

				case WM_SIZE:
				{
					UINT width = LOWORD( lParam );
					UINT height = HIWORD( lParam );

					if( wParam == SIZE_MAXIMIZED )
					{
						wind->SetWindowAttribute( _GLW_WINDOW_ICONIC, false );

						wind->SetHeight( height );
						wind->SetWidth( width );
					}
					else if( wParam == SIZE_MINIMIZED )
					{
						wind->SetWindowAttribute( _GLW_WINDOW_ICONIC, true );

						wind->SetHeight( height );
						wind->SetWidth( width );
					}
					else // wParam == SIZE_RESTORED 
					{
						wind->SetWindowAttribute( _GLW_WINDOW_ICONIC, false );

						wind->SetHeight( height );
						wind->SetWidth( width );
					}

					if( wind->m_Callbacks.ResizeCallback )
						wind->m_Callbacks.ResizeCallback( width, height );

				} break;

				case WM_EXITSIZEMOVE:
				{
					UINT width = LOWORD( lParam );
					UINT height = HIWORD( lParam );

					glwinResize( wind, 0, 0, width, height );

					if( wind->m_Callbacks.ResizeCallback )
						wind->m_Callbacks.ResizeCallback( wind->Width(), wind->Height() );
				} break;

				case WM_CLOSE:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_CLOSE, true );
				} break;

				case WM_DESTROY:
				{
					PostQuitMessage( 0 );
				} break;

				case WM_SETFOCUS:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_FOCUSED, true );
				} break;

				case WM_KILLFOCUS:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_FOCUSED, false );
				} break;

				// Mouse Input

				case WM_CAPTURECHANGED:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_MOUSE_CAPTURED, false );
				} break;

				case WM_LBUTTONDOWN:
				{
					wind->SetMouseDown( _GLWIN_M_LEFT );

					if( wind->m_Callbacks.MouseDownCallback )
						wind->m_Callbacks.MouseDownCallback( _GLWIN_M_LEFT );
				} break;

				case WM_LBUTTONUP:
				{
					wind->SetMouseDown( -1 );

					if( wind->m_Callbacks.MouseUpCallback )
						wind->m_Callbacks.MouseUpCallback( _GLWIN_M_LEFT );
				} break;

				case WM_RBUTTONDOWN:
				{
					wind->SetMouseDown( _GLWIN_M_LEFT );

					if( wind->m_Callbacks.MouseDownCallback )
						wind->m_Callbacks.MouseDownCallback( _GLWIN_M_RIGHT );
				} break;

				case WM_RBUTTONUP:
				{
					wind->SetMouseDown( -1 );

					if( wind->m_Callbacks.MouseUpCallback )
						wind->m_Callbacks.MouseUpCallback( _GLWIN_M_RIGHT );
				} break;

				case WM_MBUTTONDOWN:
				{
					wind->SetMouseDown( _GLWIN_M_MIDDLE );

					if( wind->m_Callbacks.MouseDownCallback )
						wind->m_Callbacks.MouseDownCallback( _GLWIN_M_MIDDLE );
				} break;

				case WM_MBUTTONUP:
				{
					wind->SetMouseDown( -1 );

					if( wind->m_Callbacks.MouseUpCallback )
						wind->m_Callbacks.MouseUpCallback( _GLWIN_M_MIDDLE );
				} break;

				case WM_MOUSEHOVER:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_MOUSE_HOVERED, true );
				} break;

				case WM_MOUSELEAVE:
				{
					wind->SetWindowAttribute( _GLW_WINDOW_MOUSE_HOVERED, false );
				} break;

				case WM_MOUSEMOVE:
				{
					float x = GET_X_LPARAM( lParam );
					float y = GET_Y_LPARAM( lParam );

					if( wind->m_Callbacks.MouseMovedCallback )
						wind->m_Callbacks.MouseMovedCallback( x, y );
				} break;

				// KB INPUT
				
				// there HAS to be a better way
				// TODO: ^, is kind of fixed.
				case WM_KEYDOWN:
				{
					int glKey = Win32KeyToGlwin( ( int )wParam );

					if( wind->m_Callbacks.KeyDownCallback )
						wind->m_Callbacks.KeyDownCallback( glKey );

				} break;

				case WM_KEYUP:
				{
					int glKey = Win32KeyToGlwin( ( int )wParam );

					if( wind->m_Callbacks.KeyReleaseCallback )
						wind->m_Callbacks.KeyReleaseCallback( glKey );

				} break;

				default:
				{
					break;
				}
			}
		}

		return DefWindowProc( hwnd, uMsg, wParam, lParam );
	}

#endif

	void RegisterWindowClass()
	{
	#if defined( _WIN32 )
		WNDCLASS window_class ={ 0 };

		window_class.lpfnWndProc = WindowProc;
		window_class.hInstance = GetModuleHandleW( NULL );
		window_class.hCursor = LoadCursor( nullptr, IDC_ARROW );
		window_class.hbrBackground = ( HBRUSH )COLOR_WINDOW;
		window_class.lpszClassName = _GLW_CLASS_NAME;

		RegisterClass( &window_class );
	#endif
	}

	void UnregisterWindowClass()
	{
	#if defined( _WIN32 )
		UnregisterClassW( _GLW_CLASS_NAME, GetModuleHandleW( NULL ) );
	#endif
	}

	glwin::Window* glwinCreateWindow( const std::string& title, int width, int height, int x, int y, WindowStyle style /*= WindowStyle::NORMAL */, WindowGraphicsAPI graphicsAPI /*= WindowGraphicsAPI::OPENGL*/ )
	{
		CreateLibrary();

	#if defined( _WIN32 )

		static Window* wind = new Window( title, width, height );

		RegisterWindowClass();

		if( x == 0 && y == 0 )
		{
			int nx = ( GetSystemMetrics( SM_CXSCREEN ) - width ) / 2;
			int ny = ( GetSystemMetrics( SM_CYSCREEN ) - height ) / 2;

			x = nx;
			y = ny;
		}

		wind->Library().Handle = CreateWindowExW
		(
			WS_EX_APPWINDOW,
			_GLW_CLASS_NAME,
			StringToWString( title ).c_str(),
			wind->Library().Windows32ToGLWin( style ),
			x, y,
			width, height,
			nullptr, nullptr, GetModuleHandleW( NULL ), nullptr
		);

		switch( style )
		{
			case glwin::WindowStyle::EMPTY:
			{
				int currentStyle = GetWindowStyle( wind->Library().Handle, GWL_STYLE );

				SetWindowLong( wind->Library().Handle, GWL_STYLE, ( currentStyle & ~( WS_CAPTION | WS_SIZEBOX ) ) );
			} break;
		}

		wind->Library().GraphicsAPI = graphicsAPI;
		wind->Library().ShouldLoadGraphicsAPI = graphicsAPI != glwin::WindowGraphicsAPI::NONE;

		wind->Library().Instance = GetModuleHandleW( NULL );

		SetPropW( wind->Library().Handle, L"GLWIN", wind );

		wind->CreateWindowProc();

		DragAcceptFiles( wind->Library().Handle, TRUE );

		_GLW_ASSERT( IsWindow( wind->Library().Handle ) ); // The window is invalid.

		ShowWindow( wind->Library().Handle, SW_SHOW );

		return wind;

	#elif defined( __linux__ )
		static glwin::Window* wind = new glwin::Window( title, width, height );

		wind->Library().xDisplay = XOpenDisplay( 0 );

		wind->Library().Connection = XGetXCBConnection( wind->Library().xDisplay );

		wind->Library().DefaultScreen = DefaultScreen( wind->Library().xDisplay );

		XSetEventQueueOwner( wind->Library().xDisplay, XCBOwnsEventQueue );

		/* Get the first screen */
		wind->Library().Screen = xcb_setup_roots_iterator( xcb_get_setup( wind->Library().Connection ) ).data;

		wind->Library().Handle = xcb_generate_id( wind->Library().Connection );

		wind->Library().GraphicsAPI = graphicsAPI;
		wind->Library().ShouldLoadGraphicsAPI = graphicsAPI != glwin::WindowGraphicsAPI::NONE;

		glwinLinuxInitGLXContext( wind );

		uint32_t eventmask = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS;
		uint32_t valuemask = XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;
		uint32_t valuelist[] ={ eventmask, wind->Library().ColorMap, 0 };

		/* Create the window */
		xcb_create_window(
			wind->Library().Connection,
			XCB_COPY_FROM_PARENT,
			wind->Library().Handle,
			wind->Library().Screen->root,
			x, y,
			width, height, 10,
			XCB_WINDOW_CLASS_INPUT_OUTPUT,
			wind->Library().VisualID,
			valuemask,
			valuelist );

		xcb_change_property( wind->Library().Connection,
			XCB_PROP_MODE_REPLACE,
			wind->Library().Handle,
			XCB_ATOM_WM_NAME,
			XCB_ATOM_STRING,
			8,
			strlen( title.c_str() ),
			title.c_str() );

		xcb_change_property( wind->Library().Connection,
			XCB_PROP_MODE_REPLACE,
			wind->Library().Handle,
			XCB_ATOM_WM_ICON_NAME,
			XCB_ATOM_STRING,
			8,
			strlen( title.c_str() ),
			title.c_str() );

		wind->Library().Atom_Protocals = xcb_intern_atom_reply( wind->Library().Connection, xcb_intern_atom( wind->Library().Connection, 1, strlen( "WM_PROTOCOLS" ), "WM_PROTOCOLS" ), NULL );

		wind->Library().Atom_CloseWindow = xcb_intern_atom_reply( wind->Library().Connection, xcb_intern_atom( wind->Library().Connection, 1, strlen("WM_DELETE_WINDOW"), "WM_DELETE_WINDOW" ), NULL );

		xcb_change_property( wind->Library().Connection,
			XCB_PROP_MODE_REPLACE,
			wind->Library().Handle,
			wind->Library().Atom_Protocals,
			XCB_ATOM_ATOM,
			32,
			1,
			&wind->Library().Atom_CloseWindow );

		xcb_map_window( wind->Library().Connection, wind->Library().Handle );

		xcb_flush( wind->Library().Connection );

		return wind;

	#else

		wind = new Window( false ); //-V779

		return wind;

	#endif
	}

	glwin::Window* glwinCreateWindow( WindowCreateInfo& createInfo )
	{
		return glwinCreateWindow( createInfo.Title, createInfo.Width, createInfo.Height, createInfo.X, createInfo.Y, createInfo.Style );
	}

	void glwinDestroyWindow( glwin::Window* window )
	{
		window->m_Callbacks ={};

		glwinDestroyContext( window );

	#if defined( _WIN32 )

		DestroyWindow( window->Library().Handle );

		window->Library().Instance = NULL;


	#elif defined( __linux__ )

		xcb_destroy_window( window->Library().Connection, window->Library().Handle );

	#endif
		window->m_Handle ={};
		delete window;
	}

	// VS Formatting does not work here

#if defined( _WIN32 )

	void glwinCreateContext( glwin::Window* wind )
	{
		// https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-choosepixelformat

		PIXELFORMATDESCRIPTOR pfd{};

		if ( !wind->Library().ShouldLoadGraphicsAPI || wind->Library().GraphicsAPI > glwin::WindowGraphicsAPI::OPENGL )
			return;

		if ( wind->Library().GraphicsAPI == glwin::WindowGraphicsAPI::OPENGL )
		{
			pfd ={
				sizeof( PIXELFORMATDESCRIPTOR ),  //  size of this pfd  
				1,                     // version number  
				PFD_DRAW_TO_WINDOW |   // support window  
				PFD_SUPPORT_OPENGL |   // support OpenGL  
				PFD_DOUBLEBUFFER,      // double buffered  
				PFD_TYPE_RGBA,         // RGBA type  
				24,                    // 24-bit color depth  
				0, 0, 0, 0, 0, 0,      // color bits ignored  
				0,                     // no alpha buffer  
				0,                     // shift bit ignored  
				0,                     // no accumulation buffer  
				0, 0, 0, 0,            // accum bits ignored  
				32,                    // 32-bit z-buffer      
				0,                     // no stencil buffer  
				0,                     // no auxiliary buffer  
				PFD_MAIN_PLANE,        // main layer  
				0,                     // reserved  
				0, 0, 0                // layer masks ignored  
			};
		}
		else
		{
			pfd ={
				sizeof( PIXELFORMATDESCRIPTOR ),  //  size of this pfd  
					1,                     // version number  
					PFD_DRAW_TO_WINDOW |   // support window  
					PFD_DOUBLEBUFFER,      // double buffered  
					PFD_TYPE_RGBA,         // RGBA type  
					24,                    // 24-bit color depth  
					0, 0, 0, 0, 0, 0,      // color bits ignored  
					0,                     // no alpha buffer  
					0,                     // shift bit ignored  
					0,                     // no accumulation buffer  
					0, 0, 0, 0,            // accum bits ignored  
					32,                    // 32-bit z-buffer      
					0,                     // no stencil buffer  
					0,                     // no auxiliary buffer  
					PFD_MAIN_PLANE,        // main layer  
					0,                     // reserved  
					0, 0, 0                // layer masks ignored  
			};
		}

		HDC dc = GetDC( wind->NativeWindow() );

		int  iPixelFormat;
		iPixelFormat = ChoosePixelFormat( dc, &pfd );
		SetPixelFormat( dc, iPixelFormat, &pfd );

		HGLRC rc = wglCreateContext( dc );
		wglMakeCurrent( dc, rc );

		wind->Library().DC = dc;
		wind->Library().RenderContext = rc;

		wind->Library().GLInstance = _load_library( "opengl32.dll" );

		// Load function ptrs
		{
			typedef const uint8_t* ( APIENTRYP _PFNGLGETSTRINGPROC ) ( unsigned int name );
			_PFNGLGETSTRINGPROC glGetString = NULL;

			void* ffnc = ( void* )wglGetProcAddress( "glGetString" );

			if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
			{
				ffnc = ( void* )GetProcAddress( wind->Library().GLInstance, "glGetString" );
			}

			glGetString = ( _PFNGLGETSTRINGPROC )ffnc;
		
			wind->Library().GetStringFunc = glGetString;

			if( glGetString )
				glGetString = nullptr;
		}

		{
			typedef void ( APIENTRYP _PFNGLVIEWPORTPROC ) ( int x, int y, int width, int height );
			_PFNGLVIEWPORTPROC glViewport = NULL;

			void* ffnc = ( void* )wglGetProcAddress( "glViewport" );

			if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
			{
				ffnc = ( void* )GetProcAddress( wind->Library().GLInstance, "glViewport" );
			}

			glViewport = ( _PFNGLVIEWPORTPROC )ffnc;

			glViewport( 0, 0, wind->Width(), wind->Height() );

			wind->Library().ViewportFunc = glViewport;

			glViewport = nullptr;
		}

		{
			typedef void ( APIENTRYP _PFNGLCLEARPROC ) ( unsigned int mask );
			_PFNGLCLEARPROC glClear = NULL;

			typedef void ( APIENTRYP _PFNGLCLEARCOLORPROC ) ( float r, float g, float b, float a );
			_PFNGLCLEARCOLORPROC glClearColor = NULL;

			{
				void* ffnc = ( void* )wglGetProcAddress( "glClearColor" );

				if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
				{
					ffnc = ( void* )GetProcAddress( wind->Library().GLInstance, "glClearColor" );
				}

				glClearColor = ( _PFNGLCLEARCOLORPROC )ffnc;

				glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
			}

			{
				void* ffnc = ( void* )wglGetProcAddress( "glClear" );

				if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
				{
					ffnc = ( void* )GetProcAddress( wind->Library().GLInstance, "glClear" );
				}

				glClear = ( _PFNGLCLEARPROC )ffnc;

				glClear( 0x00004000 /* GL_COLOR_BUFFER_BIT */ );
			}

			wind->Library().ClearColorFunc = glClearColor;
			wind->Library().ClearFunc = glClear;

			glClearColor = nullptr;
			glClear = nullptr;
		}
	}

#endif

#if defined( __linux__ )

	void glwinLinuxInitGLXContext( glwin::Window* wind )
	{
		if ( !wind->Library().ShouldLoadGraphicsAPI )
			return;

		GLXFBConfig* fb_configs = 0;
		int num_fb_configs = 0;

		fb_configs = glXChooseFBConfig( wind->Library().xDisplay, wind->Library().DefaultScreen, wind->Library().VisualAttribs, &num_fb_configs );

		GLXFBConfig fb_config = fb_configs[ 0 ];

		glXGetFBConfigAttrib( wind->Library().xDisplay, fb_config, GLX_VISUAL_ID, &wind->Library().VisualID );

		wind->Library().glxContext = glXCreateNewContext( wind->Library().xDisplay, fb_config, GLX_RGBA_TYPE, 0, True );

		wind->Library().ColorMap = xcb_generate_id( wind->Library().Connection );
		xcb_create_colormap( wind->Library().Connection, XCB_COLORMAP_ALLOC_NONE, wind->Library().ColorMap, wind->Library().Screen->root, wind->Library().VisualID );
	}

	void glwinCreateContext( glwin::Window* wind )
	{
		GLXFBConfig* fb_configs = 0;
		int num_fb_configs = 0;

		fb_configs = glXChooseFBConfig( wind->Library().xDisplay, wind->Library().DefaultScreen, wind->Library().VisualAttribs, &num_fb_configs );

		GLXFBConfig fb_config = fb_configs[ 0 ];

		glXGetFBConfigAttrib( wind->Library().xDisplay, fb_config, GLX_VISUAL_ID, &wind->Library().VisualID );

		wind->Library().glxWindow = glXCreateWindow( wind->Library().xDisplay, fb_config, wind->Library().Handle, 0 );

		wind->Library().Drawable = wind->Library().glxWindow;

		glXMakeContextCurrent( wind->Library().xDisplay, wind->Library().Drawable, wind->Library().Drawable, wind->Library().glxContext );

		{
			typedef const uint8_t* ( APIENTRYP _PFNGLGETSTRINGPROC ) ( unsigned int name );
			_PFNGLGETSTRINGPROC glGetString = NULL;

			void* ffnc = ( void* )dlopen( "libGL.so.1", RTLD_LAZY | RTLD_LOCAL );

			if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
			{
				ffnc = ( void* )dlopen( "libGL.so", RTLD_LAZY | RTLD_LOCAL );
			}

			glGetString = ( _PFNGLGETSTRINGPROC )dlsym( ffnc, "glGetString" );

			wind->Library().GetStringFunc = glGetString;

			if( glGetString )
				glGetString = nullptr;
		}

		{
			typedef void ( APIENTRYP _PFNGLVIEWPORTPROC ) ( int x, int y, int width, int height );
			_PFNGLVIEWPORTPROC glViewport = NULL;

			void* ffnc = ( void* )dlopen( "libGL.so.1", RTLD_LAZY | RTLD_LOCAL );

			if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
			{
				ffnc = ( void* )dlopen( "libGL.so", RTLD_LAZY | RTLD_LOCAL );
			}

			glViewport = ( _PFNGLVIEWPORTPROC )dlsym( ffnc, "glViewport" );

			glViewport( 0, 0, wind->Width(), wind->Height() );

			wind->Library().ViewportFunc = glViewport;

			glViewport = nullptr;
		}

		{
			typedef void ( APIENTRYP _PFNGLCLEARPROC ) ( unsigned int mask );
			_PFNGLCLEARPROC glClear = NULL;

			typedef void ( APIENTRYP _PFNGLCLEARCOLORPROC ) ( float r, float g, float b, float a );
			_PFNGLCLEARCOLORPROC glClearColor = NULL;

			{
				void* ffnc = ( void* )dlopen( "libGL.so.1", RTLD_LAZY | RTLD_LOCAL );

				if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
				{
					ffnc = ( void* )dlopen( "libGL.so", RTLD_LAZY | RTLD_LOCAL );
				}

				glClearColor = ( _PFNGLCLEARCOLORPROC )dlsym( ffnc, "glClearColor" );

				glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
			}

			{
				void* ffnc = ( void* )dlopen( "libGL.so.1", RTLD_LAZY | RTLD_LOCAL );

				if( ffnc == 0 || ( ffnc == ( void* )0x1 ) || ( ffnc == ( void* )0x2 ) || ( ffnc == ( void* )0x3 ) || ( ffnc == ( void* )-1 ) )
				{
					ffnc = ( void* )dlopen( "libGL.so", RTLD_LAZY | RTLD_LOCAL );
				}

				glClear = ( _PFNGLCLEARPROC )dlsym( ffnc, "glClear" );

				glClear( 0x00004000 /* GL_COLOR_BUFFER_BIT */ );
			}

			wind->Library().ClearColorFunc = glClearColor;
			wind->Library().ClearFunc = glClear;

			glClearColor = nullptr;
			glClear = nullptr;
		}
	}

#endif

	void glwinDestroyContext( glwin::Window* wind )
	{
		if( wind->Library().GLInstance )
			_free_library( wind->Library().GLInstance );

	#if defined ( _WIN32 )
		wglMakeCurrent( NULL, NULL );
		ReleaseDC( wind->NativeWindow(), wind->Library().DC );
		wglDeleteContext( wind->Library().RenderContext );

	#elif defined( __linux__ )

		glXDestroyWindow( wind->Library().xDisplay, wind->Library().glxWindow );
		glXDestroyContext( wind->Library().xDisplay, wind->Library().glxContext );

	#endif
	}

	void glwinResize( glwin::Window* wind, int x, int y, int w, int h )
	{
		if( wind->Library().ViewportFunc )
			wind->Library().ViewportFunc( x, y, w, h );
	}

}