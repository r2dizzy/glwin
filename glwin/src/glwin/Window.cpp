/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "Base.h"
#include "Window.h"
// I really did not want to create a new file just for Xlib/xcb but we have.
#include "xcb/glwinXCB.h"

#if defined( __linux__ )
#include <xcb/xproto.h>
#endif

namespace glwin {

	Window::Window( std::string n, int w, int h ) 
	#if defined( _WIN32 )
		: m_Title( n ), m_Width( w ), m_Height( h ), m_WindowProc( NULL ), m_OldKeyStates()
	#elif defined( __linux__ )
		: m_Title( n ), m_Width( w ), m_Height( h ), m_OldKeyStates()
	#endif
	{
	#if defined ( _WIN32 )
		m_Handle = Windows32Library();
		m_Callbacks = WindowCallbacks();

		GetKeyboardState( m_KeyStates );

	#endif

		CreateAttributes();
	}

	void Window::CreateWindowProc()
	{
	}

	void Window::GetFramebufferSize( int* x, int* y )
	{
	#if defined ( _WIN32 )

		RECT r ={ 0, 0 };

		GetClientRect( Library().Handle, &r );

		*x = ( float )r.right;
		*y = ( float )r.bottom;

	#elif defined( __linux__ )
		*x = m_Width; 
		*y = m_Height;
	#endif
	}

	void Window::Disconnect()
	{
	#if defined( _WIN32 )

	#elif defined( __linux__ )
		xcb_disconnect( Library().Connection );
	#endif
	}

	void Window::HandleWindowInput( char key )
	{
	}

	void Window::GetMousePos( int* x, int* y )
	{
	#if defined ( _WIN32 )

		POINT pos;

		GetCursorPos( &pos );
		ScreenToClient( Library().Handle, &pos );

		*x = pos.x;
		*y = pos.y;

	#elif defined( __linux__ )

		xcb_query_pointer_cookie_t cookie;

		cookie = xcb_query_pointer( Library().Connection, Library().Handle );

		xcb_query_pointer_reply_t* reply;

		reply = xcb_query_pointer_reply( Library().Connection, cookie, NULL );

		*x = reply->root_x;
		*y = reply->root_y;
	#endif
	}

	void Window::SetMousePos( int x, int y )
	{
	#if defined ( _WIN32 )

		POINT newPos = { x, y };

		SetCursorPos( x, y );
		ScreenToClient( Library().Handle, &newPos );

	#elif defined( __linux__ )

		xcb_warp_pointer( Library().Connection, XCB_NONE, XCB_NONE, 0, 0, 0, 0, x, y );

	#endif
	}

	void Window::CaptureMouse()
	{
	#if defined ( _WIN32 )
		SetCapture( Library().Handle );

		SetWindowAttribute( _GLW_WINDOW_MOUSE_CAPTURED, true );

	#elif defined( __linux__ )
		SetWindowAttribute( _GLW_WINDOW_MOUSE_CAPTURED, true );
	#endif
	}

	void Window::UncaptureMouse()
	{
	#if defined ( _WIN32 )
		ReleaseCapture();

		SetWindowAttribute( _GLW_WINDOW_MOUSE_CAPTURED, false );

	#elif defined( __linux__ )
	#endif
	}

	void Window::HideMouse()
	{
	#if defined ( _WIN32 )
		ShowCursor( false );
	#elif defined( __linux__ )
		XFixesHideCursor( Library().xDisplay, Library().Handle );
		XFLush( Library().xDisplay );
	#endif
	}

	void Window::ShowMouse()
	{
	#if defined ( _WIN32 )
		ShowCursor( true );
	#elif defined( __linux__ )
		XFixesShowCursor( Library().xDisplay, Library().Handle );
		XFLush( Library().xDisplay );
	#endif
	}

	// Was glwin::ResetMouse
	void Window::NormalMouse()
	{
	#if defined ( _WIN32 )

		ShowMouse();

		_GLW_ASSERT( Library().Cursors.find( "IDC_ARROW" ) != Library().Cursors.end() ); // Could not find the cursor int the map

		Library().CurrentCursor = Library().Cursors[ "IDC_ARROW" ];
	#endif
	}

	void Window::SetMouseDown( int button )
	{
		if( button == 141 || button == 151 || button == 161 || button == -1 )
			Library().MouseDown = button;
	}

	void Window::UpdateInput()
	{
	#if defined ( _WIN32 )
	#endif
	}

	void Window::Update()
	{
	#if defined( _WIN32 )

		MSG msg = {};

		// TODO: SetWindow width ( but then again we ever we resize windows handles it )

		while( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) > 0 )
		{
			TranslateMessage( &msg );

		#define _GLW_CLEAR_ON_UPDATE 1
		#if defined ( _GLW_CLEAR_ON_UPDATE )
			if ( Library().ShouldLoadGraphicsAPI )
			{
				Library().ClearColorFunc( 0.0f, 0.0f, 0.0f, 0.0f );
				Library().ClearFunc( 0x00004000 );
			}
		#endif

			DispatchMessage( &msg );

			UpdateInput();
		}


	#elif defined( __linux__ )
		
		glwinCheckEvent( this );

		#define _GLW_CLEAR_ON_UPDATE 1
		#if defined ( _GLW_CLEAR_ON_UPDATE )
		if( Library().ShouldLoadGraphicsAPI )
		{
			Library().ClearColorFunc( 0.0f, 0.0f, 0.0f, 0.0f );
			Library().ClearFunc( 0x00004000 );
		}
		#endif


	#endif
	}

	void Window::SwapBuffers() 
	{
	#if defined( _WIN32 )
		::SwapBuffers( Library().DC );

	#elif defined( __linux__ )

		glXSwapBuffers( Library().xDisplay, Library().Drawable );

	#endif
	}

}