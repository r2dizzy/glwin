/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "glwinXCB.h"
#include "glwin/Base.h"
#include "glwin/Window.h"

#include "glwin/Creators.h"

#if defined ( __linux__ )

namespace glwin {

	void WindowProc( Window* wind, xcb_connection_t* connection, xcb_window_t window )
	{
		xcb_generic_event_t* event = xcb_wait_for_event( connection );

		if( event && wind )
		{
			switch( _GLW_TO_RESPONSE( event, 0x80 ) )
			{
				case XCB_EXPOSE:
				{
					// Swap buffers but the user should do that.
				} break;

				case XCB_RESIZE_REQUEST:
				{
					xcb_resize_request_event_t* resizeEvent = ( xcb_resize_request_event_t* )event;

					wind->SetHeight( resizeEvent->height );
					wind->SetWidth( resizeEvent->width );

					glwinResize( wind, 0, 0, wind->Width(), wind->Height() );

					if( wind->m_Callbacks.ResizeCallback )
						wind->m_Callbacks.ResizeCallback( wind->Width(), wind->Height() );
				} break;

				case XCB_CLIENT_MESSAGE: 
				{
					xcb_intern_atom_cookie_t cookie2 = xcb_intern_atom( c, 0, 16, "WM_DELETE_WINDOW" );
					xcb_intern_atom_reply_t* reply2 = xcb_intern_atom_reply( c, cookie2, 0 );

					if( ( *( xcb_client_message_event_t* )event ).data.data32[ 0 ] == ( *reply2 ).atom ) 
					{
						wind->SetWindowAttribute( _GLW_WINDOW_CLOSE, true );
					}
				}

				default:
					break;
			}
		}
		else
			BreakDebug();
	}

	void glwinCheckEvent( Window* wind )
	{
		WindowProc( wind, wind->Library().Connection, wind->Library().Handle );
	}
}

#endif