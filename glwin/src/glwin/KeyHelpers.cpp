/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#include "KeyHelpers.h"

namespace glwin {

#if defined( _WIN32 )

	int Win32KeyToGlwin( int ccx )
	{
		switch( ccx )
		{
			// Arrow Keys

			case VK_LEFT:
			{
				return _GLWIN_LEFT_KEY;
			} break;

			case VK_RIGHT:
			{
				return _GLWIN_RIGHT_KEY;
			} break;

			case VK_UP:
			{
				return _GLWIN_UP_KEY;
			} break;

			case VK_DOWN:
			{
				return _GLWIN_DOWN_KEY;
			} break;

			// 6 set of keys above arrow keys

			case VK_INSERT:
			{
				return _GLWIN_INS_KEY;
			} break;

			case VK_DELETE:
			{
				return _GLWIN_DEL_KEY;
			} break;

			case VK_END:
			{
				return _GLWIN_END_KEY;
			} break;

			case VK_HOME:
			{
				return _GLWIN_HOME_KEY;
			} break;

			case VK_PRIOR:
			{
				return _GLWIN_PG_KEY;
			} break;

			case VK_NEXT:
			{
				return _GLWIN_PD_KEY;
			} break;

			// 3 set of keys above the 6 set of keys above the arrow keys

			case VK_SNAPSHOT:
			{
				return _GLWIN_PRINT_SC_KEY;
			} break;

			case VK_SCROLL:
			{
				return _GLWIN_SCR_IK_KEY;
			} break;

			case VK_PAUSE: // It might not be the pause break key tho
			{
				return _GLWIN_PAUSE_BRK_KEY;
			} break;


			// numpad keys

			case VK_NUMPAD0:
			{
				return _GLWIN_INS_NUMPAD_KEY;
			} break;

			case VK_NUMPAD1:
			{
				return _GLWIN_NUMPAD_1_KEY;
			} break;

			case VK_NUMPAD2:
			{
				return _GLWIN_NUMPAD_2_KEY;
			} break;

			case VK_NUMPAD3:
			{
				return _GLWIN_NUMPAD_3_KEY;
			} break;

			case VK_NUMPAD4:
			{
				return _GLWIN_NUMPAD_4_KEY;
			} break;

			case VK_NUMPAD5:
			{
				return _GLWIN_NUMPAD_5_KEY;
			} break;

			case VK_NUMPAD6:
			{
				return _GLWIN_NUMPAD_6_KEY;
			} break;

			case VK_NUMPAD7:
			{
				return _GLWIN_NUMPAD_7_KEY;
			} break;

			case VK_NUMPAD8:
			{
				return _GLWIN_NUMPAD_8_KEY;
			} break;

			case VK_NUMPAD9:
			{
				return _GLWIN_NUMPAD_9_KEY;
			} break;

			case VK_MULTIPLY:
			{
				return _GLWIN_STAR_NUMPAD_KEY;
			} break;

			case VK_ADD:
			{
				return _GLWIN_PLUS_NUMPAD_KEY;
			} break;

			case VK_SUBTRACT:
			{
				return _GLWIN_DASH_NUMPAD_KEY;
			} break;

			case VK_DECIMAL:
			{
				return _GLWIN_DEL_NUMPAD_KEY;
			} break;

			case VK_DIVIDE:
			{
				return _GLWIN_FORWARDSLASH_NUMPAD_KEY;
			} break;

			case VK_SEPARATOR:
			{
				return _GLWIN_DEL_NUMPAD_KEY;
			} break;

			// Func keys

			case VK_F1:
			{
				return _GLWIN_F1_KEY;
			} break;

			case VK_F2:
			{
				return _GLWIN_F2_KEY;
			} break;

			case VK_F3:
			{
				return _GLWIN_F3_KEY;
			} break;

			case VK_F4:
			{
				return _GLWIN_F4_KEY;
			} break;

			case VK_F5:
			{
				return _GLWIN_F5_KEY;
			} break;

			case VK_F6:
			{
				return _GLWIN_F6_KEY;
			} break;

			case VK_F7:
			{
				return _GLWIN_F7_KEY;
			} break;

			case VK_F8:
			{
				return _GLWIN_F8_KEY;
			} break;

			case VK_F9:
			{
				return _GLWIN_F9_KEY;
			} break;

			case VK_F10:
			{
				return _GLWIN_F10_KEY;
			} break;

			case VK_F11:
			{
				return _GLWIN_F11_KEY;
			} break;

			case VK_F12:
			{
				return _GLWIN_F12_KEY;
			} break;

			// ESC key

			case VK_ESCAPE:
			{
				return _GLWIN_ESC_KEY;
			} break;

			// Numbers

			case 0x31:
			{
				return _GLWIN_1_KEY;
			} break;

			case 0x32:
			{
				return _GLWIN_2_KEY;
			} break;

			case 0x33:
			{

				return _GLWIN_3_KEY;
			} break;

			case 0x34:
			{

				return _GLWIN_4_KEY;
			} break;

			case 0x35:
			{

				return _GLWIN_5_KEY;
			} break;

			case 0x36:
			{

				return _GLWIN_6_KEY;
			} break;

			case 0x37:
			{

				return _GLWIN_7_KEY;
			} break;

			case 0x38:
			{

				return _GLWIN_8_KEY;
			} break;

			case 0x39:
			{

				return _GLWIN_9_KEY;
			} break;

			case 0x30:
			{

				return _GLWIN_0_KEY;
			} break;

			// others

			case VK_SELECT:
			{

				return _GLWIN_MENU_KEY;
			} break;

			case VK_CLEAR:
			{

				return _GLWIN_MENU_KEY;
			} break;

			case VK_RETURN:
			{

				return _GLWIN_ENTER_KEY;
			} break;

			case VK_SHIFT:
			{

				return _GLWIN_SHIFT_KEY;
			} break;

			case VK_CONTROL:
			{

				return _GLWIN_L_CTRL_KEY;
			} break;

			case VK_TAB:
			{

				return _GLWIN_TAB_KEY;
			} break;

			case VK_BACK:
			{

				return _GLWIN_BACKSPACE_KEY;
			} break;

			case VK_MENU:
			{

				return _GLWIN_L_ALT_KEY;
			} break;

			case VK_CAPITAL:
			{

				return _GLWIN_CAPS_KEY;
			} break;

			case VK_RWIN:
			case VK_LWIN:
			{

				return _GLWIN_UNIQUE_KEY;
			} break;

			case VK_OEM_1:
			{

				return _GLWIN_SEMICOLON_ALT_KEY;
			} break;

			case VK_OEM_2:
			{

				return _GLWIN_FORWARDSLASH_KEY;
			} break;

			case VK_OEM_3:
			{

				return _GLWIN_HASHTAG_KEY;
			} break;

			case VK_OEM_4:
			{

				return _GLWIN_OPEN_BRACKET_KEY;
			} break;

			case VK_OEM_5:
			{

				return _GLWIN_BLACKSLASH_KEY;
			} break;

			case VK_OEM_6:
			{

				return _GLWIN_CLOSE_BRACKET_KEY;
			} break;

			case VK_OEM_7:
			{

				return _GLWIN_AT_KEY;
			} break;

			case VK_OEM_PLUS:
			{

				return _GLWIN_EQUALS_KEY;
			} break;

			case VK_OEM_COMMA:
			{

				return _GLWIN_COMMA_KEY;
			} break;

			case VK_OEM_MINUS:
			{

				return _GLWIN_DASH_KEY;
			} break;

			case VK_OEM_PERIOD:
			{
				return _GLWIN_PERIOD_KEY;
			} break;

			// letter keys

			case 0x41:
			{
				return _GLWIN_A_KEY;
			} break;

			case 0x42:
			{
				return _GLWIN_B_KEY;
			} break;

			case 0x43:
			{
				return _GLWIN_C_KEY;
			} break;

			case 0x44:
			{
				return _GLWIN_D_KEY;
			} break;

			case 0x45:
			{
				return _GLWIN_E_KEY;
			} break;

			case 0x46:
			{
				return _GLWIN_F_KEY;
			} break;

			case 0x47:
			{
				return _GLWIN_G_KEY;
			} break;

			case 0x48:
			{
				return _GLWIN_H_KEY;
			} break;

			case 0x49:
			{
				return _GLWIN_I_KEY;
			} break;

			case 0x4A:
			{
				return _GLWIN_J_KEY;
			} break;

			case 0x4B:
			{
				return _GLWIN_K_KEY;
			} break;

			case 0x4C:
			{
				return _GLWIN_L_KEY;
			} break;

			case 0x4D:
			{
				return _GLWIN_M_KEY;
			} break;

			case 0x4E:
			{
				return _GLWIN_N_KEY;
			} break;

			case 0x4F:
			{
				return _GLWIN_O_KEY;
			} break;

			case 0x50:
			{
				return _GLWIN_P_KEY;
			} break;

			case 0x51:
			{
				return _GLWIN_Q_KEY;
			} break;

			case 0x52:
			{
				return _GLWIN_R_KEY;
			} break;

			case 0x53:
			{
				return _GLWIN_S_KEY;
			} break;

			case 0x54:
			{
				return _GLWIN_T_KEY;
			} break;

			case 0x55:
			{
				return _GLWIN_U_KEY;
			} break;

			case 0x56:
			{
				return _GLWIN_V_KEY;
			} break;

			case 0x57:
			{
				return _GLWIN_W_KEY;
			} break;

			case 0x58:
			{
				return _GLWIN_X_KEY;
			} break;

			case 0x59:
			{
				return _GLWIN_Y_KEY;
			} break;

			case 0x5A:
			{
				return _GLWIN_Z_KEY;
			} break;

			default:
				return	-1;
		}
	}

#endif
}