-- Only for building it from the repo it self
workspace "GLwin"
	architecture "x64"
	-- Again Only for building it from the repo it self
	startproject "glwin-debugger"
	targetdir "build"

	configurations
	{
		"Debug",
		"Release"
	}
	
	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "glwin"
	location "glwin"
	kind "StaticLib"
	language "C++"
	cppdialect "C++20"
	staticruntime "on"
	
	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS",
		"_GLW_OPENGL_NONE"
	}

	includedirs
	{
		"%{prj.name}/src"
	}

	filter "system:windows"
		systemversion "latest"

		links 
		{
			"opengl32.lib",
			"dwmapi"
		}

		defines
		{
		}

		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"


	filter "system:linux"
		systemversion "latest"

		links 
		{
			"stdc++fs",
			"pthread",
			"dl",
			"xcb",
			"X11",
			"X11-xcb",
			"GL"
		}

		defines
		{
		}

		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"

	filter "system:macosx"
		systemversion "11.0"

		links 
		{
		}

		defines
		{
		}

		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"


-- Only for testing
project "glwin-debugger"
	location "glwin-debugger"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++20"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	defines
	{
		"_CRT_SECURE_NO_WARNINGS"
	}

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"%{prj.name}/src",
		"glwin/src",
		"glwin/src/glwin"
	}

	links
	{
		"glwin"
	}

	filter "system:windows"
		systemversion "latest"

		links 
		{
			"opengl32.lib",
			"dwmapi"
		}

		defines
		{
		}
		
		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"

	filter "system:linux"
		systemversion "latest"

		links 
		{
			"stdc++fs",
			"pthread",
			"dl",
			"xcb",
			"X11",
			"X11-xcb",
			"GL"
		}

		defines
		{
		}

		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"

	filter "system:macosx"
		systemversion "11.0"

		links 
		{
		}

		defines
		{
		}

		filter "configurations:Debug"
			defines "_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "_RELEASE"
			runtime "Release"
			optimize "on"



