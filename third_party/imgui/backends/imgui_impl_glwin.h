// dear imgui: Platform Backend for glwin

#pragma once
#include "imgui.h"      // IMGUI_IMPL_API

IMGUI_IMPL_API bool     ImGui_ImplGlwin_InitForOpenGL( void* window );
IMGUI_IMPL_API void     ImGui_ImplGlwin_Shutdown();
IMGUI_IMPL_API void     ImGui_ImplGlwin_NewFrame();