/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2021 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#pragma once

#include <stdint.h>

#if defined( __linux__ )
#include <signal.h> /* SIGEV_* */
#include <time.h>
#endif

namespace glwin {

	struct Timer
	{
		Timer();
		~Timer();

		uint64_t Offset; // Only used in Win32 but we'll keep it

	// I'm not that fond of posix so this could be bad.
	#if !defined ( _POSIX_C_SOURCE  ) && defined ( _GLWIN_USE_POSIX )
	#error _POSIX_C_SOURCE must be defined if you are using posix.
	#elif defined ( _POSIX_C_SOURCE  ) && defined ( _GLWIN_USE_POSIX )
		clockid_t ClockId;
		timer_t TimerId;
	#endif

		uint64_t Frequency;
	};

	extern void InitTimer      ( Timer& t );
	extern void CloseTimer     ( Timer& t );
	extern uint64_t TimerValue ( Timer& t );

	extern double GetTime();
}